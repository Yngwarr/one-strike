const config = {
    type: Phaser.AUTO,
    width: WIDTH,
    height: HEIGHT,
    backgroundColor: '#008080',
    physics: {
        default: 'matter',
        matter: { plugins: { wrap: true } }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

let ship;
let foes = [];
let cursors;
let emerald;

let pointer;

let cat_chain;
let cat_weapons;
let cat_ships;

let lives = MAX_LIVES;
let lives_view;
let score = 0;
let score_view;

let scene;

function init() {
    let game = new Phaser.Game(config);
    score_view = document.getElementById('score');
    lives_view = document.getElementById('lives');
}

function preload () {
    this.load.path = 'img/';
    this.load.image('ship', 'ship.png');
    this.load.image('link', 'link.png');
    this.load.image('emerald', 'emerald.png');
    this.load.image('void', 'void.png');
    this.load.image('dead', 'dead.png');

    this.load.image('bg1', 'bg1.png');
    this.load.image('bg2', 'bg2.png');
    this.load.image('bg3', 'bg3.png');
    this.load.image('bg4', 'bg4.png');
    this.load.image('bg5', 'bg5.png');
    this.load.image('bg6', 'bg6.png');
    this.load.image('bg7', 'bg7.png');
    this.load.image('bg8', 'bg8.png');
    this.load.image('bg9', 'bg9.png');
    this.load.image('bg10', 'bg10.png');
}

function create () {
    scene = this;

    this.anims.create({
        key: 'tube',
        frames: [
            {key: 'bg1'},
            {key: 'bg2'},
            {key: 'bg3'},
            {key: 'bg4'},
            {key: 'bg5'},
            {key: 'bg6'},
            {key: 'bg7'},
            {key: 'bg8'},
            {key: 'bg9'},
            {key: 'bg10'},
        ],
        framerate: 8,
        repeat: -1
    });
    this.add.sprite(400, 300, 'bg1').play('tube');

    // collision categories
    cat_chain = this.matter.world.nextCategory();
    cat_weapons = this.matter.world.nextCategory();
    cat_ships = this.matter.world.nextCategory();

    ship = this.matter.add.image(0, 0, 'ship', null, {
        ignoreGravity: true,
        mass: SHIP_MASS
    });
    ship.setFixedRotation();
    ship.setBounce(.6);
    ship.body.label = 'p';
    ship.setCollisionCategory(cat_ships);

    pointer = this.matter.add.image(0, 0, 'void', null, {
        ignoreGravity: true,
        mass: 1000
    });
    this.matter.add.joint(pointer, ship, POINTER_LEN, POINTER_STIFF);
    pointer.setCollidesWith([]);

    // player's chain
    let y = 0;
    let prev = ship;
    let link;
    for (let i = 0; i < CHAIN_LENGTH; ++i) {
        link = this.matter.add.image(0, y, 'link', null, {
            shape: 'circle',
            mass: LINK_MASS
        });
        this.matter.add.joint(prev, link, JOINT_DELTA + ((i === 0) ? 5 : 0), JOINT_STIFF);
        link.setFixedRotation();
        link.setCollisionCategory(cat_chain);
        link.setCollidesWith([cat_chain]);

        prev = link;
        y += 16;
    }

    // player's weapon
    emerald = this.matter.add.image(0, 50, 'emerald', null, {
        shape: 'circle',
        mass: EMERALD_MASS
    });
    this.matter.add.joint(prev, emerald, JOINT_DELTA + 15, JOINT_STIFF);
    emerald.body.label = 'w';
    emerald.setCollisionCategory(cat_weapons);
    emerald.setCollidesWith([cat_ships, cat_weapons]);

    for (let i = 0; i < INIT_FOES; ++i) {
        foes.push(spawn_random_foe());
    }

    this.matter.world.on('collisionstart', collide);
    timedEvent = this.time.addEvent({
        delay: MV_DELAY,
        callback: mv_foes,
        callbackScope: this,
        loop: true
    });

    cursors = this.input.keyboard.createCursorKeys();
}

function roll(a, b) {
    return Phaser.Math.RND.between(a, b);
}

let global_count = 0;
function update(time, delta) {
    const pos = this.input.activePointer.position;
    pointer.setPosition(pos.x, pos.y);
    
    let count = 0;
    for (let i = 0; i < foes.length; ++i) {
        if (foes[i].body.label === 'd' && foes[i].y > HEIGHT) {
            foes[i].destroy();
            delete foes[i];
            ++count;
        }
    }
    foes = foes.filter(f => { return f; });
    global_count += count;
    for (let i = 0; i < count + ((global_count / PLUS_FOE)|0); ++i) {
        foes.push(spawn_random_foe());
    }
    global_count %= 10;
}

function mv_foes() {
    for (let i = 0; i < foes.length; ++i) {
        if (!foes[i]) continue;
        foes[i].applyForce({x: roll(-FORCE_EDGE, FORCE_EDGE), y: roll(-FORCE_EDGE, FORCE_EDGE)});
    }
}

function spawn_random_foe() {
    const x = Phaser.Math.RND.between(0, WIDTH);
    const y = Phaser.Math.RND.between(0, HEIGHT);
    return spawn_foe(x, y);
}

function spawn_foe(x, y) {
    let foe = scene.matter.add.image(x, y, 'ship', null, {
        ignoreGravity: true,
        mass: FOE_MASS,
        plugin: WRAP_BOUNDS
    });
    foe.setFixedRotation();
    foe.body.label = 'e';
    foe.setCollisionCategory(cat_ships);
    foe.setCollidesWith([cat_ships, cat_weapons]);
    return foe;
}

function collide(e, a, b) {
    if (cld(a, b, 'we')) {
        const v = c_get('w', a, b).velocity;
        if (norm(v) >= KILL_EDGE) {
            let foe = c_get('e', a, b).gameObject;
            foe.applyForce({x: v.x, y: v.y});
            foe.setIgnoreGravity(false);
            foe.body.label = 'd';
            score_inc();
            console.log('kill');
        } else {
            console.log('ouch');
        }
    } else if (cld(a, b, 'de')) {
        let alive = c_get('e', a, b).gameObject;
        alive.setIgnoreGravity(false);
        alive.body.label = 'd';
        score_inc();
    } else if (cld(a, b, 'wp')) {
        console.log('hurt');
    } else if (cld(a, b, 'pe')) {
        console.log('dead');
        --lives;
        lives_view.innerText = `x ${lives}`;
        if (lives <= 0) {
            console.log('Game over!');
        }
    }
}

function score_inc() {
    score += SCORE_DELTA;
    score_view.innerText = `Score: ${score}`;
}

function cld(a, b, pair) {
    return (a.label === pair[0] && b.label === pair[1])
        || (a.label === pair[1] && b.label === pair[0]);
}

function c_get(ch, a, b) {
    return a.label === ch ? a : b;
}

function norm(vec) {
    return Math.sqrt(vec.x**2 + vec.y**2);
}
