const WIDTH = 800;
const HEIGHT = 600;

const SHIP_MASS = 100;
const FOE_MASS = 500;
const LINK_MASS = 0.5;
const EMERALD_MASS = 150;

const CHAIN_LENGTH = 2;
const MV_DELAY = 3000;
const PLUS_FOE = 10;
const MAX_LIVES = 3;
const SCORE_DELTA = 5;

// 0 - 1
const JOINT_STIFF = 1;
const JOINT_DELTA = 16;

const POINTER_STIFF = .5;
const POINTER_LEN = 1;

const KILL_EDGE = 8;
const FORCE_EDGE = 4;
const INIT_FOES = 5;

const WRAP_BOUNDS = {
    wrap: {
        min: {
            x: 0,
            y: 0
        },
        max: {
            x: 800,
            y: 600
        }
    }
};
